package net.iescierva.ajfp.mislugaresjava2021.presentacion;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import net.iescierva.ajfp.mislugaresjava2021.R;
import net.iescierva.ajfp.mislugaresjava2021.casos_de_uso.CasosUsoLugar;
import net.iescierva.ajfp.mislugaresjava2021.databinding.EdicionLugarBinding;
import net.iescierva.ajfp.mislugaresjava2021.datos.CustomApplication;
import net.iescierva.ajfp.mislugaresjava2021.modelo.Lugar;
import net.iescierva.ajfp.mislugaresjava2021.modelo.RepositorioLugares;
import net.iescierva.ajfp.mislugaresjava2021.modelo.TipoLugar;

public class EdicionLugarActivity extends AppCompatActivity {
    private EditText nombre;
    private Spinner tipo;
    private EditText direccion;
    private EditText telefono;
    private EditText url;
    private EditText comentario;
    private int pos;
    private CasosUsoLugar usosLugar;
    private Lugar lugar;
    net.iescierva.ajfp.mislugaresjava2021.databinding.EdicionLugarBinding binding;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = EdicionLugarBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        nombre = binding.nombreLugar;
        direccion = binding.direccion;
        telefono = binding.telefono;
        comentario = binding.edComentario;
        url = binding.url;

        RepositorioLugares lugares = ((CustomApplication) getApplication()).getLugares();

        //Debe iniciarse la actividad con el id pasado desde otra actividad
        pos = getIntent().getExtras().getInt("pos", 0);
        lugar = lugares.get_element(pos);
        usosLugar = new CasosUsoLugar(this, lugares);
        // Asignamos el lugar antes para poder mostrar su tipo en el spinner
        crearSpinner();
        actualizaVistas();
    }

    private void actualizaVistas() {
        nombre.setText(lugar.getNombre());
        direccion.setText(lugar.getDireccion());
        telefono.setText(Integer.toString(lugar.getTelefono()));
        comentario.setText(lugar.getComentario());
        url.setText(lugar.getUrl());
    }

    private void crearSpinner(){
        tipo = binding.tipo;
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, TipoLugar.getNombres());
        adaptador.setDropDownViewResource(android.R.layout.
                simple_spinner_dropdown_item);
        tipo.setAdapter(adaptador);
        tipo.setSelection(lugar.getTipo().ordinal());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edicion_lugar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if (item.getItemId() == findViewById(R.id.accion_guardar).getId() ){
            lugar.setNombre(nombre.getText().toString());
            lugar.setTipo(TipoLugar.values()[tipo.getSelectedItemPosition()]);
            lugar.setDireccion(direccion.getText().toString());
            lugar.setTelefono(Integer.parseInt(telefono.getText().toString()));
            lugar.setUrl(url.getText().toString());
            lugar.setComentario(comentario.getText().toString());
            usosLugar.guardar(pos, lugar);
            MainActivity.getInstancia().crearListaLugares();
        } else if (item.getItemId() == R.id.accion_buscar) {
            String nombre = lugar.getNombre();
            Uri uri = Uri.parse("https://www.google.com/search?q=" + nombre);
            Intent gSearchIntent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(gSearchIntent);
        }
        finish();
        return true;
    }
}
