package net.iescierva.ajfp.mislugaresjava2021.casos_de_uso;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import net.iescierva.ajfp.mislugaresjava2021.presentacion.AcercaDeActivity;
import net.iescierva.ajfp.mislugaresjava2021.presentacion.PreferenciasActivity;

public class CasosUsoActividades {
    //El contexto es desde donde se abre
    public static void lanzarAcercaDe(Context context){
        Intent i = new Intent(context, AcercaDeActivity.class);
        context.startActivity(i);
    }

    public static void lanzarPreferencias(Context context){
        Intent i = new Intent(context, PreferenciasActivity.class);
        context.startActivity(i);
    }

    public static void lanzarVistaLugar(CasosUsoLugar usosLugar, Context context){
        final EditText entrada = new EditText(context);
        entrada.setText("0");
        new AlertDialog.Builder(context)
                .setTitle("Selección de lugar")
                .setMessage("indica su id:")
                .setView(entrada)
                .setPositiveButton("Ok", (dialog, whichButton) -> {
                    int id = Integer.parseInt (entrada.getText().toString());
                    usosLugar.mostrar(id);
                })
                .setNegativeButton("Cancelar", null)
                .show();
    }

    public static void mostrarPreferencias(Context context){
        //Devuelve el diccionario de las preferencias que hemos definido en preferencias.xml
        SharedPreferences pref =
                PreferenceManager.getDefaultSharedPreferences(context);
        String s =  String.format("%-50s %s %-27s %s %-47s %s %-41s %s %-52s %s",
                "Notificaciones: \t", pref.getBoolean("notificaciones",true),
                "\nE-mail: \t", pref.getString("email","")
                ,"\nTipos de Notificaciones: \t", pref.getString("tiposNotificaciones", "0")
                ,"\nMáximo de lugares a mostrar: \t" , pref.getString("maximo","?")
                ,"\nOrden de los lugares: \t", pref.getString("orden",""));
        Toast.makeText(context, s, Toast.LENGTH_LONG).show();
    }

    public static void salir(Activity context){
        context.finish();
    }
}
