package net.iescierva.ajfp.mislugaresjava2021.presentacion;

import android.os.Bundle;
//Extender AppCompatActivity es necesario para utilizar la Toolbar
import androidx.appcompat.app.AppCompatActivity;

import net.iescierva.ajfp.mislugaresjava2021.R;

public class AcercaDeActivity extends AppCompatActivity {
    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acercade);
    }
}
