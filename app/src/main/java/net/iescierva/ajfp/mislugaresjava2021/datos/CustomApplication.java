package net.iescierva.ajfp.mislugaresjava2021.datos;

import android.app.Application;
import android.content.res.Configuration;

import net.iescierva.ajfp.mislugaresjava2021.presentacion.AdaptadorLugares;
import net.iescierva.ajfp.mislugaresjava2021.modelo.LugaresLista;
import net.iescierva.ajfp.mislugaresjava2021.modelo.RepositorioLugares;

public class CustomApplication extends Application {
        private final RepositorioLugares lugares = new LugaresLista();
        public AdaptadorLugares adaptador = new AdaptadorLugares(lugares);
        // Called when the application is starting, before any other application objects have been created.
        // Overriding this method is totally optional!
        @Override
        public void onCreate() {
            super.onCreate();
            // Required initialization logic here!
        }

        // Called by the system when the device configuration changes while your component is running.
        // Overriding this method is totally optional!
        @Override
        public void onConfigurationChanged(Configuration newConfig) {
            super.onConfigurationChanged(newConfig);
        }

        // This is called when the overall system is running low on memory,
        // and would like actively running processes to tighten their belts.
        // Overriding this method is totally optional!
        @Override
        public void onLowMemory() {
            super.onLowMemory();
        }

        public RepositorioLugares getLugares() {
            return lugares;
        }
}

