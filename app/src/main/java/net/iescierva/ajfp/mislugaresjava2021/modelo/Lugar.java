package net.iescierva.ajfp.mislugaresjava2021.modelo;

public class Lugar {
    private String nombre; //Añadido
    private String direccion; //Añadido
    private GeoPunto posicion;
    private TipoLugar tipo; //Añadido
    private String foto;
    private int telefono; //Añadido
    private String url; //Añadido
    private String comentario; //Añadido
    private long fecha; //Añadido
    private float valoracion; //Añadido

    //Casteo de String a Enum es TipoLugar.valueOf(String). La valoración y la foto no están añadidas en el formulario

    public Lugar(String nombre, String direccion, double latitud, double longitud, String foto, TipoLugar tipo, int telefono, String url, String comentario, int valoracion) {
        fecha = System.currentTimeMillis();
        posicion = new GeoPunto(latitud, longitud);
        this.foto = foto;
        this.tipo=tipo;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.url = url;
        this.comentario = comentario;
        this.valoracion = valoracion;
    }
    //constructor alternativo recibiendo GeoPunto en lugar de latitud y longitud
    public Lugar(String nombre, String direccion, GeoPunto p, String foto, TipoLugar tipo, int telefono, String url, String comentario, int valoracion) {
        this(nombre,direccion, p.getLatitud(), p.getLongitud(),
                foto, tipo, telefono, url, comentario, valoracion);
    }

    public Lugar()
    {
        this("","",0,0,"", TipoLugar.OTROS,0,"","",0);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public GeoPunto getPosicion() {
        return posicion;
    }

    public void setPosicion(GeoPunto posicion) {
        this.posicion = posicion;
    }

    public TipoLugar getTipo() {
        return tipo;
    }

    public void setTipo(TipoLugar tipo) {
        this.tipo = tipo;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public long getFecha() {
        return fecha;
    }

    public void setFecha(long fecha) {
        this.fecha = fecha;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public float getValoracion() {
        return valoracion;
    }

    public void setValoracion(float valoracion) {
        this.valoracion = valoracion;
    }

    @Override
    public String toString() {
        return "Lugar{" +
                "nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'' +
                ", posicion=" + posicion +
                ", tipo=" + tipo +
                ", telefono=" + telefono +
                ", url='" + url + '\'' +
                ", comentario='" + comentario + '\'' +
                ", fecha=" + fecha +
                ", valoracion=" + valoracion +
                '}';
    }
}
