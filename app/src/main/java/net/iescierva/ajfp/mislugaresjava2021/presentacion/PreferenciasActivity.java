package net.iescierva.ajfp.mislugaresjava2021.presentacion;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class PreferenciasActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //El FragmentManager utiliza una transacción para introducir y ordenar los fragmentos
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new PreferenciasFragment())
                //.replace(new Fragment())
                .commit();
    }
}
