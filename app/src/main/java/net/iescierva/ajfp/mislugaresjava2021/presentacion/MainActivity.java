package net.iescierva.ajfp.mislugaresjava2021.presentacion;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.iescierva.ajfp.mislugaresjava2021.R;
import net.iescierva.ajfp.mislugaresjava2021.casos_de_uso.CasosUsoActividades;
import net.iescierva.ajfp.mislugaresjava2021.casos_de_uso.CasosUsoLugar;
import net.iescierva.ajfp.mislugaresjava2021.databinding.ActivityMainBinding;
import net.iescierva.ajfp.mislugaresjava2021.datos.CustomApplication;
import net.iescierva.ajfp.mislugaresjava2021.modelo.RepositorioLugares;

import java.lang.ref.WeakReference;

public class MainActivity extends AppCompatActivity {
    public static WeakReference<MainActivity> weakActivity;
    final int PERMISSION_ALL = 1;
    public AdaptadorLugares adaptador;
    String[] PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private RecyclerView recyclerView;
    private MediaPlayer mp;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.ItemDecoration separador;
    private CasosUsoLugar usosLugar;
    private RepositorioLugares lugares;

    //Este método sólo devuelve true si todos los condicionales de permiso no aceptado son falsos
    public static boolean hasPermissions(Context context, String[] permissions) {
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    public static MainActivity getInstancia() {
        return weakActivity.get();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        weakActivity = new WeakReference<>(this);
        net.iescierva.ajfp.mislugaresjava2021.databinding.ActivityMainBinding binding =
                ActivityMainBinding.inflate(getLayoutInflater());
        //Nos devuelve siempre el layout de la actividad del binding: ActivityMain
        setContentView(binding.getRoot());

        mp = MediaPlayer.create(this, R.raw.prueba);
        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();

        //Obtendremos los lugares lo primero ya que CustomApplication se ejecuta la primera
        lugares = ((CustomApplication) getApplication()).getLugares();
        usosLugar = new CasosUsoLugar(this, lugares);

        //Acceder a la vista "recycler_view" de la actividad actual (MainActivity)
        recyclerView = (RecyclerView) binding.includeContentMain.recyclerView;

        crearListaLugares();

        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
    }

    public void crearListaLugares() {
        //Construir el adaptador y asignarlo al recyclerView
        adaptador = new AdaptadorLugares(this, lugares);
        recyclerView.setAdapter(adaptador);

        //Construir y asignar el LayoutManager, en este caso de tipo Linear
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //Construir y asignar el separador de elementos
        separador = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(separador);

        adaptador.setOnItemClickListener(v -> {
            int pos = recyclerView.getChildAdapterPosition(v);
            usosLugar.mostrar(pos);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            //Le podemos pasar una vista si queremos que se llegue a la actividad con una vista
            CasosUsoActividades.lanzarPreferencias(this);
            return true;
        }
        if (id == R.id.acercaDe) {
            CasosUsoActividades.lanzarAcercaDe(this);
            return true;
        }
        if (id == R.id.menu_buscar) {
            CasosUsoActividades.lanzarVistaLugar(usosLugar, this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mp.start();
        Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mp.start();
        Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show();
        super.onPause();
    }

    @Override
    protected void onStop() {
        mp.pause();
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mp.start();
        Toast.makeText(this, "onRestart", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle estadoGuardado) {
        super.onSaveInstanceState(estadoGuardado);
        if (mp != null) {
            int pos = mp.getCurrentPosition();
            estadoGuardado.putInt("posicion", pos);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle estadoGuardado) {
        super.onRestoreInstanceState(estadoGuardado);
        if (estadoGuardado != null && mp != null) {
            int pos = estadoGuardado.getInt("posicion");
            mp.seekTo(pos);
        }
    }
}

